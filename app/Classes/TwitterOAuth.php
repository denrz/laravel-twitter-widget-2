<?php

namespace App\Classes;

class TwitterOAuth
{
    private $api_key;
    private $api_secret_key;
    private $access_token;
    private $access_token_secret;
    // https://developer.twitter.com/en/docs/basics/authentication/overview/application-only

    public function __construct($config)
    {
        if (!is_array($config)) {
            throw new \Exception('Invalid Auth parameters');
        }

        $this->api_key = $config['api_key'];
        $this->api_secret_key = $config['api_secret_key'];
        $this->access_token = $config['access_token'];
        $this->access_token_secret = $config['access_token_secret'];
    }

    public function getTimeline($name, $count)
    {
        $bearer_token = $this->getBearerToken();

        $curl = curl_init();

        $headers = array();
        $headers[] = 'Authorization: Bearer ' . $bearer_token;

        curl_setopt_array($curl,
            array(
                CURLOPT_URL => 'https://api.twitter.com/1.1/statuses/user_timeline.json?count=' . $count . '&screen_name=' . $name,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_HTTPHEADER => $headers
            )
        );

        $feed = curl_exec($curl);
        if(curl_errno($curl)){
            throw new \Exception(curl_error($curl));
        }

        curl_close($curl);
        return json_decode($feed);
    }

    private function getBearerToken()
    {
        if (session()->has('twitter_bearer_token')) {
            return session('twitter_bearer_token');
        }

        $url = 'https://api.twitter.com/oauth2/token';

        $curl = curl_init();

        $headers = array();
        $headers[] = 'Content-Type: application/x-www-form-urlencoded;charset=UTF-8';
        $headers[] = 'Authorization: Basic ' . base64_encode(urlencode($this->api_key) . ':' . urlencode($this->api_secret_key));

        curl_setopt_array($curl,
            array(
                CURLOPT_URL => $url,
                CURLOPT_POST => 1,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_POSTFIELDS => 'grant_type=client_credentials',
                CURLOPT_HTTPHEADER => $headers
            )
        );

        $resp = curl_exec($curl);
        if(curl_errno($curl)){
            throw new \Exception(curl_error($curl));
        }

        curl_close($curl);
        $resp = json_decode($resp);

        if (!$resp || !isset($resp->token_type) || !isset($resp->access_token)) {
            throw new \Exception('Failed to get Bearer Token');
        }

        if ($resp->token_type !== 'bearer') {
            throw new \Exception('Invalid token type');
        }

        session(['twitter_bearer_token' => $resp->access_token]);
        return $resp->access_token;
    }
}
