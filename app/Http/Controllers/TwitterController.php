<?php

namespace App\Http\Controllers;

use App\Classes\TwitterOAuth;

class TwitterController extends Controller
{
    public function getTimeline($name, $count)
    {
        if (!isset($name)) {
            $returnData = array(
                'status' => 'error',
                'message' => 'Name parameter is missing!'
            );
            return response()->json($returnData, 400);
        }

        // Limit tweets number to 10
        if (!isset($count) || $count < 1 || $count > 10) {
            $count = 10;
        }

        try {
            $twitter = new TwitterOAuth(config('twitter.config'));
            $timeline = $twitter->getTimeline($name, $count);

            // Validate response
            if (isset($timeline->errors)) {
                return response()->json($timeline->errors, 400);
            }
        } catch (\Exception $e) {
            $returnData = array(
                'status' => 'error',
                'message' => $e->getMessage()
            );
            return response()->json($returnData, 500);
        }

        return response()->json([
            // todo save to Cache if will be used by multiple users to save the requests
            $this->filterTimeline($timeline)
        ]);
    }

    private function filterTimeline($timeline)
    {
        $filtered_timeline = [];

        foreach ($timeline as $tweet) {
            $filtered_tweet = [
                'tweet_id' => $tweet->id_str,
                'created_at' => date('jS M, Y - G:i', strtotime($tweet->created_at)),
                'text' => $tweet->text,
                'user_id' => $tweet->user->id,
                'name' => $tweet->user->name,
                'screen_name' => $tweet->user->screen_name,
                'profile_image' => $tweet->user->profile_image_url,
                'retweet_count' => $tweet->retweet_count,
                'favorite_count' => $tweet->favorite_count,
                'in_reply_to_user_id' => $tweet->in_reply_to_user_id,
                'in_reply_to_screen_name' => $tweet->in_reply_to_screen_name
            ];
            array_push($filtered_timeline, $filtered_tweet);
        }
        return $filtered_timeline;
    }
}
