<?php

return [
    'config' => [
        'api_key' => env('TWITTER_API_KEY', null),
        'api_secret_key' => env('TWITTER_API_SECRET_KEY', null),
        'access_token' => env('TWITTER_API_TOKEN', null),
        'access_token_secret' => env('TWITTER_API_TOKEN_SECRET', null)
    ]
];